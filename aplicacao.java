import java.util.Arrays;

// Classe abstrata Veículo com método clone e represent
abstract class Veiculo implements Cloneable {
    protected String modelo;
    protected String marca;
    protected String cor;
    protected int numeroRodas;

    public Veiculo(String modelo, String marca, String cor, int numeroRodas) {
        this.modelo = modelo;
        this.marca = marca;
        this.cor = cor;
        this.numeroRodas = numeroRodas;
    }

    // Método clone abstrato
    public abstract Veiculo clone();

    // Método represent abstrato
    public abstract String represent();
}

// Subclasse Carro que estende Veiculo
class Carro extends Veiculo {
    private int numeroPortas;

    public Carro(String modelo, String marca, String cor, int numeroRodas, int numeroPortas) {
        super(modelo, marca, cor, numeroRodas);
        this.numeroPortas = numeroPortas;
    }

    // Implementação do método clone para Carro
    @Override
    public Carro clone() {
        return new Carro(this.modelo, this.marca, this.cor, this.numeroRodas, this.numeroPortas);
    }

    // Implementação do método represent para Carro
    @Override
    public String represent() {
        return "Carro [Modelo: " + modelo + ", Marca: " + marca + ", Cor: " + cor +
                ", Número de Rodas: " + numeroRodas + ", Número de Portas: " + numeroPortas + "]";
    }
}

// Subclasse Moto que estende Veiculo
class Moto extends Veiculo {
    private boolean temCarenagem;

    public Moto(String modelo, String marca, String cor, int numeroRodas, boolean temCarenagem) {
        super(modelo, marca, cor, numeroRodas);
        this.temCarenagem = temCarenagem;
    }

    // Implementação do método clone para Moto
    @Override
    public Moto clone() {
        return new Moto(this.modelo, this.marca, this.cor, this.numeroRodas, this.temCarenagem);
    }

    // Implementação do método represent para Moto
    @Override
    public String represent() {
        return "Moto [Modelo: " + modelo + ", Marca: " + marca + ", Cor: " + cor +
                ", Número de Rodas: " + numeroRodas + ", Tem Carenagem: " + temCarenagem + "]";
    }
}

// Classe Aplicação
public class Aplicacao {
    public static void main(String[] args) {
        // Criação de uma array com seis veículos usando o método clone
        Veiculo[] veiculos = new Veiculo[6];
        veiculos[0] = new Carro("Sedan", "Toyota", "Azul", 4, 4);
        veiculos[1] = new Carro("Hatchback", "Ford", "Vermelho", 4, 2);
        veiculos[2] = new Moto("Esportiva", "Honda", "Preto", 2, true);
        veiculos[3] = new Carro("SUV", "Chevrolet", "Prata", 4, 5);
        veiculos[4] = new Moto("Custom", "Harley-Davidson", "Preto", 2, false);
        veiculos[5] = new Carro("Caminhonete", "Nissan", "Verde", 4, 4);

        // Chama o método que retorna um array de clones dos veículos
        Veiculo[] clones = cloneVeiculos(veiculos);

        // Imprime a representação de cada elemento do array de clones
        for (Veiculo clone : clones) {
            System.out.println(clone.represent());
        }
    }

    // Método que retorna um array de clones dos veículos
    private static Veiculo[] cloneVeiculos(Veiculo[] veiculos) {
        Veiculo[] clones = new Veiculo[veiculos.length];

        for (int i = 0; i < veiculos.length; i++) {
            clones[i] = veiculos[i].clone();
        }

        return clones;
    }
}
